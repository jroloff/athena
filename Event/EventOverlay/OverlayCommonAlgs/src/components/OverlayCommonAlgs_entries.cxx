#include "../CopyJetTruthInfo.h"
#include "../CopyMcEventCollection.h"
#include "../CopyTimings.h"

#include "../BSFilter.h"
#include "../ByteStreamMultipleOutputStreamCopyTool.h"

DECLARE_COMPONENT( CopyJetTruthInfo )
DECLARE_COMPONENT( CopyMcEventCollection )
DECLARE_COMPONENT( CopyTimings )

DECLARE_COMPONENT( BSFilter )
DECLARE_COMPONENT( ByteStreamMultipleOutputStreamCopyTool )
