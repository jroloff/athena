# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration.
#
# File: AthenaKernel/python/StoreID.py
# Created: Jul 2017, sss
# Purpose: Define symbols for StoreGate StoreIDs.
#
# THIS FILE MUST BE KEPT SYNCHRONIZED WITH AthenaKernel/StoreID.h.
#

EVENT_STORE     = 0
DETECTOR_STORE  = 1
CONDITION_STORE = 2
METADATA_STORE  = 3
SIMPLE_STORE    = 4
SPARE_STORE     = 5
PILEUP_STORE    = 6
UNKNOWN         = 7
